/* Nama : Ryan Fadhillah
   NIM  : 20/457221/SV/17668
   Calon Programmer Heroes */


/* define untuk mendeklarasikan pin yang digunakan
   pada arduino uno pin 7 adalah pin digital
   sedangkan pin 6 adalah pin PWM */ 

#define pinmotor1 6
#define pinmotor2 7
int pwm=127; /*nilai pwm bisa diubah dari -255 sampai 255 
untuk mengatur kecepatan motor dc*/


void setup() {
  // kode pada bagian setup akaan dijalankan sekali
  // pinmode untuk setting pin agar dia menjadi OUTPUT/INPUT
  pinMode(pinmotor1, OUTPUT);
  pinMode(pinmotor2, OUTPUT);
  
  /* membuka atau mengaktifkan port serial dengan kecepatan 
  data rate 9600 bps */
  Serial.begin(9600);
}

void loop() {
  if (pwm >= 0 && pwm <= 255) {
     digitalWrite(pinmotor1, HIGH);
     analogWrite(pinmotor2, pwm);
     //apabila nilai pwm positif maka motor berputar misal ke kanan
  }
  else if (pwm < 0 && pwm >= -255) {
     digitalWrite(pinmotor1, HIGH);
     analogWrite(pinmotor2, -1*pwm);
     //maka selain kondisi di atas maka motor berputar lawannya
  }
}
